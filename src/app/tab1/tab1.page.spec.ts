import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavController } from 'ionic-angular';
import { DetailPageModule } from '../detail/detail.module';


export class Tab1Page {

  constructor(public navCtrl:NavController){

  }

  public pushDetailPage() {
    this.navCtrl.push('../detail/detail.page.html');
  }
}


describe('Tab1Page', () => {
  let component: Tab1Page; 
  let fixture: ComponentFixture<Tab1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Tab1Page],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Tab1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
 
